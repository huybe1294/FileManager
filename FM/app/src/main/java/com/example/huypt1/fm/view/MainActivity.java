package com.example.huypt1.fm.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.huypt1.fm.R;
import com.example.huypt1.fm.adapter.FileAdapter;
import com.example.huypt1.fm.base.BaseActivity;
import com.example.huypt1.fm.manager.FileManager;
import com.example.huypt1.fm.model.FileItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {
    private static final int REQUEST_CODE_PERMISSION = 100;
    private FileManager fileManager;
    private List<FileItem> fileItems;
    private FileAdapter fileAdapter;
    private RecyclerView recyclerView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreateBaseActivity() {
        fileManager = new FileManager();
        requestPermissions();
        initRecycleFile();
    }

    @Override
    protected void registerListener() {

    }

    private void initRecycleFile() {
        initData();

        recyclerView= (RecyclerView) findViewById(R.id.rcv_file);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        fileAdapter = new FileAdapter(this, fileItems);
        recyclerView.setAdapter(fileAdapter);
    }

    private void initData() {
        fileItems = new ArrayList<>();
        List<FileItem> listFile = fileManager.getListFile();

        for (int i = 0; i < listFile.size(); i++) {
            fileItems.add(listFile.get(i));
        }

    }

    private void requestPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                fileManager.readExternalStorage();
            } else {
                String[] permission = new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE};
                requestPermissions(permission, REQUEST_CODE_PERMISSION);
            }
        } else {
            fileManager.readExternalStorage();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isPermissionGranted(String permission) {
        return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                Toast.makeText(this, "please grant permission", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
