package com.example.huypt1.fm.model;

/**
 * Created by huypt1 on 11/4/2017.
 */

public class FileItem {
    private int avatar;
    private String fileName;
    private long size;
    private String numberItem;
    private String timeUpdate;
    private String iconNext;
    private String iconCheck;

    public FileItem() {
    }

    public FileItem(String fileName, long size, String timeUpdate) {
        this.fileName = fileName;
        this.size = size;
        this.timeUpdate = timeUpdate;
    }

    public FileItem(int avatar, String fileName, long size, String numberItem, String timeUpdate, String iconNext, String iconCheck) {
        this.avatar = avatar;
        this.fileName = fileName;
        this.size = size;
        this.numberItem = numberItem;
        this.timeUpdate = timeUpdate;
        this.iconNext = iconNext;
        this.iconCheck = iconCheck;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getNumberItem() {
        return numberItem;
    }

    public void setNumberItem(String numberItem) {
        this.numberItem = numberItem;
    }

    public String getTimeUpdate() {
        return timeUpdate;
    }

    public void setTimeUpdate(String timeUpdate) {
        this.timeUpdate = timeUpdate;
    }

    public String getIconNext() {
        return iconNext;
    }

    public void setIconNext(String iconNext) {
        this.iconNext = iconNext;
    }

    public String getIconCheck() {
        return iconCheck;
    }

    public void setIconCheck(String iconCheck) {
        this.iconCheck = iconCheck;
    }
}
