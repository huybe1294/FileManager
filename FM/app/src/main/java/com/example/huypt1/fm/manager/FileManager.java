package com.example.huypt1.fm.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.example.huypt1.fm.model.FileItem;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by belos on 11/2/2017.
 */

public class FileManager {
    private static final String TAG = "FileManager";
    private File[] files;
    private List<FileItem> listFile = new ArrayList<>();

    public FileManager() {
    }

    public void readExternalStorage() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();

        File file = new File(path);
        files = file.listFiles();
        for (int i = 0; i < files.length; i++) {
            String name = files[i].getName();
            long zise = files[i].length();
            Date lastMidified=new Date(files[i].lastModified());
            SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String formattedDateString= dateFormat.format(lastMidified);
            listFile.add(new FileItem(name, zise, formattedDateString));

            if (files[i].isDirectory()) {

            }
            if (files[i].isFile()) {

            }
        }
    }

    public List<FileItem> getListFile() {
        return listFile;
    }
}
