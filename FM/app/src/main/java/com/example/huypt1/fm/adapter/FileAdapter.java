package com.example.huypt1.fm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.huypt1.fm.R;
import com.example.huypt1.fm.model.FileItem;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by huypt1 on 11/4/2017.
 */

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {
    private Context context;
    private List<FileItem> fileItems;
    private LayoutInflater inflater;

    public FileAdapter(Context context, List<FileItem> fileItems) {
        this.context = context;
        this.fileItems = fileItems;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_file, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FileItem itemView = fileItems.get(position);

        holder.imgAvatar.setImageResource(R.drawable.ic_folder);
        holder.txtFileName.setText(itemView.getFileName());
        holder.txtSize.setText(String.valueOf(itemView.getSize()/1024));
        holder.txtTimeUpdate.setText(String.valueOf(itemView.getTimeUpdate()));
        holder.imgNext.setImageResource(R.drawable.ic_navigate_next_grey_500_24dp);
    }

    @Override
    public int getItemCount() {
        return fileItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgAvatar;
        private TextView txtFileName;
        private TextView txtSize;
        private TextView txtTimeUpdate;
        private ImageView imgNext;

        public ViewHolder(View itemView) {
            super(itemView);

            imgAvatar = itemView.findViewById(R.id.img_avatar);
            txtFileName = itemView.findViewById(R.id.txt_file_name);
            txtSize = itemView.findViewById(R.id.txt_size);
            txtTimeUpdate = itemView.findViewById(R.id.txt_time_update);
            imgNext = itemView.findViewById(R.id.img_next);

        }
    }
}
